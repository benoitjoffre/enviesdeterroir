<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuInfo extends Model
{
    protected $fillable = [
        'titre',
        'prix'
      ];
}
