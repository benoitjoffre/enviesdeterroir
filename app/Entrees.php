<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrees extends Model
{
    protected $fillable = [
        'nom',
        'nom_anglais',
        'description',
        'description_anglais',
        'fait_maison',
        'prix'
    ];
}
