<?php

namespace App\Http\Controllers;
use App\menuDessert;
use Illuminate\Http\Request;

class MenuDessertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu_dessert = menuDessert::all();

        return view('menus.index', compact('menu_dessert'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu_dessert.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom'=>'required',
            'nom_anglais'=>'',
            'description'=>'',
            'description_anglais'=>'',
            'fait_maison'=>''
          ]);
          $menu_dessert = new menuDessert([
            'nom' => $request->get('nom'),
            'nom_anglais' => $request->get('nom_anglais'),
            'description' => $request->get('description'),
            'description_anglais' => $request->get('description_anglais'),
            'fait_maison' => $request->get('fait_maison')
          ]);
          $menu_dessert->save();
          return redirect('/menus')->with('success', "Le fromage ou dessert du menu à bien été ajouté");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $menu_dessert = menuDessert::find($id);

        return view('menu_dessert.edit', compact('menu_dessert'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nom'=>'required',
            'nom_anglais'=>'',
            'description'=>'',
            'description_anglais'=>'',
            'fait_maison' =>''
          ]);
    
          $menu_dessert = menuDessert::find($id);
          $menu_dessert->nom = $request->get('nom');
          $menu_dessert->nom_anglais = $request->get('nom_anglais');
          $menu_dessert->description = $request->get('description');
          $menu_dessert->description_anglais = $request->get('description_anglais');
          $menu_dessert->fait_maison = $request->get('fait_maison');
          $menu_dessert->save();
    
          return redirect('/menus')->with('success', "Le fromage ou dessert du menu à bien été mis à jour");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu_dessert = menuDessert::find($id);
        $menu_dessert->delete();

    return redirect('/menus')->with("success", "Le fromage ou dessert a bien été suprimé");
    }
}
