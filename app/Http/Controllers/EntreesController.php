<?php

namespace App\Http\Controllers;
use App\Entrees;
use Illuminate\Http\Request;

class EntreesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entreess = Entrees::all();

        return view('entrees.index', compact('entreess'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('entrees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom'=>'required',
            'nom_anglais'=>'',
            'description'=>'',
            'description_anglais'=>'',
            'fait_maison'=>'',
            'prix'=>'required'
          ]);
          $entrees = new entrees([
            'nom' => $request->get('nom'),
            'nom_anglais' => $request->get('nom_anglais'),
            'description' => $request->get('description'),
            'description_anglais' => $request->get('description_anglais'),
            'fait_maison' => $request->get('fait_maison'),
            'prix' => $request->get('prix')
          ]);
          $entrees->save();
          return redirect('/entrees')->with('success', "L'élément des entrees à bien été ajouté");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entrees = entrees::find($id);

        return view('entrees.edit', compact('entrees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nom'=>'required',
            'nom_anglais'=>'',
            'description'=>'',
            'description_anglais'=>'',
            'fait_maison' =>'',
            'prix' =>''
          ]);
    
          $entrees = Entrees::find($id);
          $entrees->nom = $request->get('nom');
          $entrees->nom_anglais = $request->get('nom_anglais');
          $entrees->description = $request->get('description');
          $entrees->description_anglais = $request->get('description_anglais');
          $entrees->fait_maison = $request->get('fait_maison');
          $entrees->prix = $request->get('prix');
          $entrees->save();
    
          return redirect('/entrees')->with('success', "L'élément des entrees à bien été mis à jour");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $entrees = entrees::find($id);
    $entrees->delete();

    return redirect('/entrees')->with("success", "l'élément a bien été suprimé");
    }
}
