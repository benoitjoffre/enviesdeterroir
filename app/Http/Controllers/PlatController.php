<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plat;

class PlatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plats=Plat::All();
        return view('plats.index', compact('plats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('plats.create');
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom'=>'required|string',
            'nom_anglais'=> 'required|string',
            'description' => 'required|string',
            'description_anglais'=>'required|string',
            'prix'=>'required|string',
            'fait_maison'=>''
        ]);
        $plat = new Plat([
            'nom' => $request->get('nom'),
            'nom_anglais'=> $request->get('nom_anglais'),
            'description'=> $request->get('description'),
            'description_anglais'=>$request->get('description_anglais'),
            'prix'=>$request->get('prix'),
            'fait_maison'=>$request->get('fait_maison')
        ]);
        $plat->save();
        return redirect('/plats')->with('success', 'Le plat a été ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plat = Plat::find($id);

        return view('plats.edit', compact('plat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nom'=>'required|string',
            'nom_anglais'=> 'required|string',
            'description' => 'required|string',
            'description_anglais'=>'required|string',
            'prix'=>'required|string',
            'fait_maison'=>''
        ]);
        $plat = Plat::find($id);
        $plat->nom = $request->get('nom');
        $plat->nom_anglais = $request->get('nom_anglais');
        $plat->description = $request->get('description');
        $plat->description_anglais = $request->get('description_anglais');
        $plat->prix = $request->get('prix');
        $plat->fait_maison = $request->get('fait_maison');
        $plat->save();
        return redirect('/plats')->with('success', 'Le plat a été modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $plat = Plat::find($id);
    $plat->delete();

     return redirect('/plats')->with("success", "le plat a bien été suprimé");
    }
}
