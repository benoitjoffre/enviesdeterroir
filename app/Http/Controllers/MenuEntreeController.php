<?php

namespace App\Http\Controllers;
use App\MenuEntree;
use Illuminate\Http\Request;

class MenuEntreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu_entree = MenuEntree::all();

        return view('menus.index', compact('menu_entree'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu_entree.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom'=>'required',
            'nom_anglais'=>'',
            'description'=>'',
            'description_anglais'=>'',
            'fait_maison'=>''
          ]);
          $menu_entree = new MenuEntree([
            'nom' => $request->get('nom'),
            'nom_anglais' => $request->get('nom_anglais'),
            'description' => $request->get('description'),
            'description_anglais' => $request->get('description_anglais'),
            'fait_maison' => $request->get('fait_maison')
          ]);
          $menu_entree->save();
          return redirect('/menus')->with('success', "L'entrée du menu à bien été ajouté");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu_entree = MenuEntree::find($id);

        return view('menu_entree.edit', compact('menu_entree'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nom'=>'required',
            'nom_anglais'=>'',
            'description'=>'',
            'description_anglais'=>'',
            'fait_maison' =>''
          ]);
    
          $menu_entree = MenuEntree::find($id);
          $menu_entree->nom = $request->get('nom');
          $menu_entree->nom_anglais = $request->get('nom_anglais');
          $menu_entree->description = $request->get('description');
          $menu_entree->description_anglais = $request->get('description_anglais');
          $menu_entree->fait_maison = $request->get('fait_maison');
          $menu_entree->save();
    
          return redirect('/menus')->with('success', "L'entrée du menu à bien été mis à jour");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $menu_entree = MenuEntree::find($id);
    $menu_entree->delete();

    return redirect('/menus')->with("success", "l'entrée a bien été suprimé");
    }
}
