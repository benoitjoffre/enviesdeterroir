<?php

namespace App\Http\Controllers;
use App\MenuPlat;
use Illuminate\Http\Request;

class MenuPlatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu_plat = MenuPlat::all();

        return view('menus.index', compact('menu_plat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu_plat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom'=>'required',
            'nom_anglais'=>'',
            'description'=>'',
            'description_anglais'=>'',
            'fait_maison'=>''
          ]);
          $menu_plat = new MenuPlat([
            'nom' => $request->get('nom'),
            'nom_anglais' => $request->get('nom_anglais'),
            'description' => $request->get('description'),
            'description_anglais' => $request->get('description_anglais'),
            'fait_maison' => $request->get('fait_maison')
          ]);
          $menu_plat->save();
          return redirect('/menus')->with('success', "L'entrée du menu à bien été ajouté");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu_plat = MenuPlat::find($id);

        return view('menu_plat.edit', compact('menu_plat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nom'=>'required',
            'nom_anglais'=>'',
            'description'=>'',
            'description_anglais'=>'',
            'fait_maison' =>''
          ]);
    
          $menu_plat = MenuPlat::find($id);
          $menu_plat->nom = $request->get('nom');
          $menu_plat->nom_anglais = $request->get('nom_anglais');
          $menu_plat->description = $request->get('description');
          $menu_plat->description_anglais = $request->get('description_anglais');
          $menu_plat->fait_maison = $request->get('fait_maison');
          $menu_plat->save();
    
          return redirect('/menus')->with('success', "L'entrée du menu à bien été mis à jour");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    $menu_plat = MenuPlat::find($id);
    $menu_plat->delete();

    return redirect('/menus')->with("success", "le plat a bien été suprimé");
    }
}