<?php

namespace App\Http\Controllers;
use App\MenuInfo;
use Illuminate\Http\Request;

class MenuInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu_infos = MenuInfo::all();

        return view('menus.index', compact('menu_infos'));
        return view('menu', compact('menu_infos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titre'=>'',
            'prix'=>''
          ]);
          $menu = new MenuInfo([
            'titre' => $request->get('titre'),
            'prix' => $request->get('prix')
          ]);
          $menu->save();
          return redirect('/menus')->with('success', "Les infos du menu à bien été ajouté");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu_info = MenuInfo::find($id);

        return view('menus.edit', compact('menu_info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'titre'=>'',
            'prix'=>''
          ]);
    
          $menu_info = MenuInfo::find($id);
          $menu_info->titre = $request->get('titre');
          $menu_info->prix = $request->get('prix');
          
          $menu_info->save();
    
          return redirect('/menus')->with('success', "Les informations du menu ont bien été mises à jour");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $menu_info = MenuInfo::find($id);
    $menu_info->delete();

    return redirect('/menus')->with("success", "l'élément a bien été suprimé");
    }
}
