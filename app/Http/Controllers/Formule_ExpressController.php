<?php

namespace App\Http\Controllers;
use App\Formule_Express;
use Illuminate\Http\Request;

class Formule_ExpressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Formule_Express = Formule_Express::all();

        return view('Formule_Express.index', compact('Formule_Express'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Formule_Express.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'nom'=>'required',
            'nom_anglais'=>'',
            'description'=>'',
            'description_anglais'=>'',
            'fait_maison'=>'',
            'prix'=>'required'
          ]);
          $Formule_Express = new Formule_Express([
            'nom' => $request->get('nom'),
            'nom_anglais' => $request->get('nom_anglais'),
            'description' => $request->get('description'),
            'description_anglais' => $request->get('description_anglais'),
            'fait_maison' => $request->get('fait_maison'),
            'prix' => $request->get('prix')
          ]);
       $Formule_Express->save();
          return redirect('/Formule_Express')->with('success', "L'élément du la Formule Express à bien été ajouté");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Formule_Express = Formule_Express::find($id);

        return view('Formule_Express.edit', compact('Formule_Express'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'nom'=>'required',
            'nom_anglais'=>'',
            'description'=>'',
            'description_anglais'=>'',
            'fait_maison' =>'',
            'prix' =>''
          ]);
    
          $Formule_Express = Formule_Express::find($id);
          $Formule_Express->nom = $request->get('nom');
          $Formule_Express->nom_anglais = $request->get('nom_anglais');
          $Formule_Express->description = $request->get('description');
          $Formule_Express->description_anglais = $request->get('description_anglais');
          $Formule_Express->fait_maison = $request->get('fait_maison');
          $Formule_Express->prix = $request->get('prix');
          $Formule_Express->save();
    
          return redirect('/Formule_Express')->with('success', "L'élément du Formule_Express à bien été mis à jour");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Formule_Express = Formule_Express::find($id);
    $Formule_Express->delete();

    return redirect('/Formule_Express')->with("success", "l'élément a bien été suprimé");
    }
}
