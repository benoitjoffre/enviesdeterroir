<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/mentionslegales', function () {
    return view('components/mentionslegales');
});

Route::get('/index_anglais', function () {
    return view('index_anglais');
});
Auth::routes();

Route::get('/admin', 'AdminController@index')->name('admin');

Route::resource('menus', 'MenuController');

Route::resource('menus', 'MenuInfoController');

Route::resource('menu_entree', 'MenuEntreeController');

Route::resource('menu_plat', 'MenuPlatController');

Route::resource('menu_dessert', 'MenuDessertController');

Route::resource('plats', 'PlatController');

Route::resource('Formule_Express', 'Formule_ExpressController');

Route::resource('entrees','EntreesController');

Route::resource('plats', 'PlatController');
