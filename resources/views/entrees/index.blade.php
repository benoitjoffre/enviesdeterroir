@extends('layouts.app')

@section('content')


  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
<div class="card-columns">
        @foreach(App\entrees::all() as $entrees)
        <div class="card">
            <div class="card-header">
                <div class="card-title">Élément des entrees</div>
            </div>
            <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><p class="text-center">{{$entrees->nom}}<br>- <br>{{$entrees->nom_anglais}}</p></li>
                    </ul>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><i><p class="text-center">{{$entrees->description}} <br> - <br> {{$entrees->description_anglais}} </p></i></li>
                        @if($entrees->fait_maison == 1)
                        <li class="list-group-item crud"><img src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" width="50" height="50" alt=""></li>
                        @endif                      </ul>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><p class="text-center">{{$entrees->prix}}</li>
                    </ul>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item crud"><a href="{{ route('entrees.edit',$entrees->id)}}" class="btn btn-primary">Modifier</a>
                            
                                    <form action="{{ route('entrees.destroy', $entrees->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                             <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                                              Supprimer
                                            </button>
                                          </form>

                                          </form>

                                        </li>
                            </ul>
            </div>
        </div>
        @endforeach
        <a class="nav-link" href="{{ route('entrees.create') }}"><button class="btn btn-success">{{ __("Ajouter un élément") }}</button></a>
</div>



  {{-- <table class="table table-bordered table-hover">
    <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">Nom</th>
          <th scope="col">Nom traduit</th>
          <th scope="col">Description</th>
          <th scope="col">Description traduite</th>
          
          <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($entreess as $entrees)
        <tr>
            <th scope="row">{{$entrees->id}}</th>
            <td>{{$entrees->nom}}</td>
            <td>{{$entrees->nom_anglais}}</td>
            <td>{{$entrees->description}}</td>
            <td>{{$entrees->description_anglais}}</td>
            
            <td><a href="{{ route('entrees.edit',$entrees->id)}}" style="width:100%;" class="btn btn-primary">Edit</a>
            
                <form action="{{ route('entrees.destroy', $entrees->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" style="width:100%;" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        
        @endforeach
    </tbody>
  </table> --}}

@endsection