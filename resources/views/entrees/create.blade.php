@extends('layouts.app')
@section('content')
<div class="container create-entrees">
    <h1 class="text-center">CRÉER UN ÉLÉMENT DES ENTREES</h1>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
        <form action="{{ route('entrees.store') }}" method="post">
            <div class="form-group">
                    @csrf
                <label for="nom">Nom</label>
                <input type="text" id="nom" name="nom" class="form-control" placeholder="Entrez un élément de entrees">
            </div>

            <div class="form-group">
                <label for="nom_anglais">Nom (traduction anglaise)</label>
                <input type="text" id="nom_anglais" name="nom_anglais" class="form-control" placeholder="Entrez un élément de entrees en anglais">
            </div>
            
            <div class="form-group">
                <label for="description">Description</label>
                <input type="text" id="description" name="description" class="form-control" placeholder="Entrez une description">
            </div>


            <div class="form-group">
                <label for="description_anglais">Description (traduction anglaise)</label>
                <input type="text" id="description_anglais" name="description_anglais" class="form-control" placeholder="Entrez une description en anglais">
            </div>

            <div class="form-group">
                <label for="select">Fait maison</label>
                <input type="checkbox" value="1" checked id="fait_maison" name="fait_maison">
            </div>
            
            <div class="form-group">
                <label for="prix">Prix</label>
                <input type="text" id="prix" name="prix" class="form-control" placeholder="Entrez le prix">
            </div>

            
            <button type="submit" class="btn btn-success">Valider</button>
        </form>
</div>
@endsection
