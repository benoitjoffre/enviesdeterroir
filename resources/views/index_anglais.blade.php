<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Crimson+Text" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    
    <title>Envies de terroir Rocamadour</title>

</head>
    <body ontouchstart="">

        <div id="site">
            <header class="header">
                <div class="top">
                    <img src="https://image.noelshack.com/fichiers/2019/08/3/1550666908-logoenvt.png">
                </div>
                <hr>
            </header>
        
            <nav class="navbar navbar-expand-lg" id="navigation">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><i class="fas fa-bars"></i></button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item active"><a class="nav-link" href="#" id="accueil">Home</a></li>
                        <li class="nav-item active"><a class="nav-link" href="#" id="lacarte">The menu</a></li>
                        <li class="nav-item active"><a class="nav-link" href="#" id="rocamadour">Rocamadour</a></li>
                        <li class="nav-item active"><a class="nav-link" href="#" id="contatcetreservation">Contact & Reservation</a></li>
                    <li class="nav-item active"> <a href="{{ url('/') }}"><img src="https://d24irw6hr5upwc.cloudfront.net/1-home_default/drapeau-france-5075-cm.jpg" alt="" width="20" height="20" style="margin-top:15px;"></a></li>
                    </ul>
                </div>
            </nav>
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="bgimg">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="https://image.noelshack.com/fichiers/2019/07/2/1549985638-envies-de-terroir-038.jpg" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src=" https://image.noelshack.com/fichiers/2019/07/3/1550063085-envies-de-terroir-060.jpg" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="https://image.noelshack.com/fichiers/2019/07/2/1549985655-envies-de-terroir-002.jpg" alt="Third slide">
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <!-- 
            </div>
             -->
             
                <hr>
                <div align="center" class="texteDescriptionRestaurant" media="">
                    The brewery Envies de Terroir offers a gourmet stop in the heart of the village of Rocamadour. We greet you with a smile in a modern setting and with a convincing regional cuisine. For your dishes, we rely on quality and collaboration with local producers.                </div>
                <hr id="menu">
            </div>
        </div>

        <div class="containers" id="menu">
            <h1>The card</h1>
            <div class="carteaffichee">
                <div class="formule_express" >
                    <div class="titremenu text-center">
                        <h2 class="titre">Express <span>Formula</span></h2>
                    </div>
                    @foreach(App\Formule_Express::All() as $Formule_Express)
                    <div class="block">
                        <div class="centrercarre">
                            <div class="carrerouge"></div>
                        </div>
                        <div class="elements_blockformule">
                            <div>
                                <strong>
                                    {{$Formule_Express->nom_anglais}}, 
                                </strong>
                                {{$Formule_Express->description_anglais}}
                            </div>
                            <div class="fait_maison">
                                @if($Formule_Express->fait_maison == 1)
                                    <img src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" width="20" height="20" alt="">
                                @endif
                            </div>
                        </div>
                        <div class="prixformuleexpress text-center">
                            {{$Formule_Express->prix}} €
                        </div>
                    </div>
                    @endforeach
                    
                    <div class="menu_enfant">
                        <div class="titremenuenfant text-center">
                            <h2 class="titre">Children <span>Menu</span><span>11€</span></h2>
                        </div>
                        <div class="elementsmenuenfant">
                            <div class="carrerouge"></div>
                            Chopped steak Le Gaillard
                        </div>
                        <div class="ou">
                            <i><strong>OR</strong></i>
                        </div>
                        <div class="elementsmenuenfant">
                            <div class="carrerouge"></div>
                            Grilled duck needlefish
                        </div>
                        <div class="elementsmenuenfant">
                            French Fries
                        </div>
                        <div class="elementsmenuenfant">
                            <div class="carrerouge"></div>
                             Ice cream
                        </div>
        
                    </div>
        
                </div>
                <div class="menu text-center">
                    <div class="titremenu">
                        @foreach ( App\MenuInfo::All() as $menu_infos)
                        <h2 class="titre">{{$menu_infos->titre}} <span>{{$menu_infos->prix}} €</span></h2>    
                        @endforeach
                    </div>
                    @foreach(App\MenuEntree::All() as $menu_entree)
                    <div class="block">
                        <div class="elements_block">
                            <div>
                                <strong>
                                    {{$menu_entree->nom_anglais}}
                                </strong>
                            </div>
                            <div class="fait_maison">
                                
                                {{$menu_entree->description_anglais}}
                                <br>
                                
                                @if($menu_entree->fait_maison == 1)
                                    <img src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" width="20" height="20" alt="">
                                @endif
        
                            </div>
                        </div>
                    </div>
                    <div>
                        
                    </div>
                
                    @if(!$loop->last)
                    Or
                @endif
                    @endforeach
                    <div class="aroundcarre">
                        <div class="carrerouge"></div>
                    </div>
        
        
                    @foreach(App\MenuPlat::All() as $menu_plat)
                    <div class="block">
                        <div class="elements_block">
                            <div>
                                <strong>
                                    {{$menu_plat->nom_anglais}}
                                </strong>
                            </div>
                            <div class="fait_maison">
                                
                                {{$menu_plat->description_anglais}}
                                <br>
                                
                                @if($menu_plat->fait_maison == 1)
                                    <img src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" width="20" height="20" alt="">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div>
                        
                    </div>
                    @if(!$loop->last)
                    Or
                @endif
                    @endforeach
        
                    <div class="aroundcarre">
                        <div class="carrerouge"></div>
                    </div>
                    
                    @foreach(App\menuDessert::All() as $menu_dessert)
                    <div class="block">
                        <div class="elements_block">
                            <div>
                                <strong>
                                    {{$menu_dessert->nom_anglais}}
                                </strong>
                            </div>
                            <div class="fait_maison">
                                
                                {{$menu_dessert->description_anglais}}
                                <br>
                                
                                @if($menu_dessert->fait_maison == 1)
                                    <img src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" width="20" height="20" alt="">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div>
                        
                    </div>
                    @if(!$loop->last)
                    Or
                @endif
                    @endforeach
                    
                </div>
            </div>
        
        <div class="entreesPlats">
        
            <div class="collapse multi-collapse" id="collapse">
                <div class="entrees">
                    <div class="titremenu text-center">
                                 <h2 class="titre"> <span> Our&nbsp; </span> Starter</h2>
                            </div>
        
                            @foreach(App\Entrees::All() as $Entrees)
                                <div class="block">
                                    <div class="elements_Entrees">
        
                                        <div class="text-centre">
                                            <div class="carrerouge1"></div>
                                            <div><strong> {{$Entrees->nom_anglais}} </strong> ..... {{$Entrees->prix}} € </div>
                                                <div class="fait_maison">
                                                    <?php
                                                        if(isset($Entrees->fait_maison)){
                                                        echo'<img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">';
                                                        }
                                                    ?>
                                                </div>
        
                                        </div>
                                        <div>
                                            {{$Entrees->description_anglais}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                </div>
                
            </div>
        
            <div class="collapse multi-collapse" id="collapse">
                <div class="entrees">
                    <div class="titremenu text-center">
                                 <h2 class="titre text-center"> <span> Our&nbsp;</span> Dishes</h2>
                            </div>
        
                            @foreach(App\Plat::All() as $Plat)
                                <div class="block">
                                    <div class="elements_Entrees">
        
                                        <div class=" text-centre">
                                            <div class="carrerouge1"></div>
                                            <div><strong> {{$Plat->nom_anglais}} </strong> ..... {{$Plat->prix}} € </div>
                                                <div class="fait_maison">
                                                    <?php
                                                        if(isset($Plat->fait_maison)){
                                                        echo'<img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">';
                                                        }
                                                    ?>
                                                </div>
        
                                        </div>
                                        <div>
                                            
                                        {{$Plat->description_anglais}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                </div>
                
            </div>
        
        </div>
        
            <div class="collapse multi-collapse" id="fromage">
                <div class="fromage">
                    <div class="titreFromage">
                        <h2 class="titre"><span>Our&nbsp;</span> Cheese</h2>
                    </div>
                    <div class="elementsFromage">
                        <div class="carrerouge"></div>
                        <div>
                             Rocamadour fermier "Lacoste" and nuts salad ......... 6,00 €
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementsCollapse">
            <div class="collapse multi-collapse" id="collapse">
                <div class="desserts">
                    <div class="titreFromage">
                        <h2 class="titre"><span>Our&nbsp;</span> Desserts</h2>
                    </div>
                    <div class="carteDesserts">
                        <div class="elementsDesserts">
                            <div class="carrerouge"></div>
                            <div>
                                <strong>Prunes with red wine</strong>........5,00 €
                            </div>
                            <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                            
                        </div>
                            <div>
                                and his ice cream
                            </div>
                        <div class="elementsDesserts">
                            <div class="carrerouge"></div>
                            <div>
                                <strong>Walnut success</strong>........5,50 €
                            </div>
                            <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                            
                        </div>
                        <div class="elementsDesserts">
                            <div class="carrerouge"></div>
                            <div>
                                <strong>Mirtilles tart</strong>........5,90 €
                            </div>
                        </div>
                        <div class="elementsDesserts">
                            <div class="carrerouge"></div>
                            <div>
                                <strong>Warm apple pie</strong>........6,50 €
                            </div>
                            <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                            
                        </div>
                            <div>
                                and his vanilla ice cream
                            </div>
                        <div class="elementsDesserts">
                            <div class="carrerouge"></div>
                            <div>
                                <strong>Milk rice</strong>........5,60 €
                            </div>
                            <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                            
                        </div>
                            <div>
                                caramel salted butter or red berries
                            </div>
                        <div class="elementsDesserts">
                            <div class="carrerouge"></div>
                            <div>
                                <strong>Panna cotta</strong>........5,90 €
                            </div>
                            <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                            
                        </div>
                            
                    </div>
                </div>
            </div>
            <div class="collapse multi-collapse" id="collapse">
                <div class="crepes">
                    <div class="titreFromage">
                        <h2 class="titre"><span>Our&nbsp;</span>Crepes </h2>
                    </div>
                    <div class="carteCrepe">
                        <div class="elementsDesserts">
                            <div class="carrerouge"></div>
                            <div>
                                <strong>Sugar</strong>........2,90 €
                            </div>
                            <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                        </div>
                        <div class="elementsDesserts">
                            <div class="carrerouge"></div>
                            <div>
                                <strong>Sugar, lemon</strong>........4,20 €
                            </div>
                            <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                        </div>
                        <div class="elementsDesserts">
                            <div class="carrerouge"></div>
                            <div>
                                <strong>Jam</strong>........3,90 €
                            </div>
                            <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                        </div>
                        <div class="elementsDesserts">
                            <div class="carrerouge"></div>
                            <div>
                                <strong>Nutella</strong>........4,70 €
                            </div>
                            <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                        </div>
                        <div class="elementsDesserts">
                            <div class="carrerouge"></div>
                            <div>
                                <strong>caramel salted butter</strong>........6,00 €
                            </div>
                            <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                            
                        </div>
                            <div>
                                and his vanilla ice cream
                            </div>
                        <div class="elementsDesserts">
                            <div class="carrerouge"></div>
                            <div>
                                <strong>Grand Marnier</strong>........6,00 €
                            </div>
                            <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="row justify-content-center">
                <button class="btn btn-secondary more" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="collapse fromage">See the full card</button>
            </div>
        </div>



	
	<hr id="about" class="trait">
	<div class="imgroca">

		<div class="titre "> 
			Rocamadour
		</div>
	
				<i class="fas fa-hand-point-up"></i>
			<div class="textroca" id="textroca">
				<p>A vertiginous site!<br>
                    Rocamadour is located in the Dordogne valley: the sacred city clings to the cliff in a superposition of houses and chapels. From the castle that crowns this daring building, there is a drop of about 150 meters at the bottom of which winds Alzou Creek. On the way to Saint-Jacques-de-Compostelle, the Basilica of Saint-Sauveur and the crypt Saint Amadour, a UNESCO World Heritage Site, are open to the public after having climbed the 216 steps of the pilgrim's staircase. . The miraculous chapel, one of the seven other chapels built in the hollow of the rock, houses its jewel, the Black Virgin venerated for over a millennium.				</p>
			</div>	

			<div> </div>
	
	</div>



    <div class="allcontain vertical">
	
	<hr class="trait">
	<div class="logoref">


		<div>
			<a href="https://www.geo.fr/" target="_blank"><img class="geoguide" src="https://image.noelshack.com/fichiers/2019/07/3/1550053417-geo-guide-201512.png" alt="logo geoguide"></a>
		</div>
		<div>
			<a href="https://www.routard.com/" target="_blank"><img class="routard" src="https://image.noelshack.com/fichiers/2019/07/3/1550053468-location-recommande-guide-routard-guadeloupe2.png" alt="logo du routard"></a>
		</div>
		<div>
			<a href="https://www.lonelyplanet.fr/" target="_blank"><img class="lonelyplanet" src="https://image.noelshack.com/fichiers/2019/07/3/1550053530-lonely-planet-svg2.png" alt="logo lonely planet"></a>
		</div>	
		<div>
			<a href="https://www.petitfute.com/" target="_blank"><img class="petitfute" src="https://itctropicar.fr/arn/uploads/2019/01/logo-label-petit-fute.png" alt="logopetit fute"></a>
		</div>
	</div>

	<hr class="trait" id="footer">
	<h1 class="textContact" >Contact us & Book </h1>

	<div class="contactetmaps">

			<div class="contact">
					<h3 class="text-center"><strong>Schedule</strong></h3>
				<div class="horaires">
					
						<div class="horaireete">
								<h4>Season</h4>
								<i class="dates">( juily 1st - august 31 )</i>
								<p>
                                    From Monday to Sunday</p>
								<p>10:00 am - 10:00 pm</p>
							</div>
			
							<div class="horairehiver">
								<h4>Out of season</h4>
								<i class="dates">(September 1st - June 30)</i>
								<p>Every day except Friday</p>
								<p>10:00 am - 7:00 pm</p>
							</div>			
				</div>
				<hr class="traitseparation">
				<h3 class="text-center"><strong>Informations</strong></h3>
				<div class="infos">
						
						<div class="adresse">
								<h4>Adress</h4>
								<p > Rue de La Couronnerie <br> 46500 Rocamadour </p>
								</div>
				
								<div class="telephone">
								<h4>Phone</h4>
								<p class="margetel">05 65 33 67 38 </p>
								</div>
				
								
				</div>
				
			
			</div>
	
			<div class="maps">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d20765.69730151013!2d1.6137172714180856!3d44.80171739197678!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd7ff0106bc57a3a!2sEnvies+de+Terroir!5e0!3m2!1sfr!2sfr!4v1549981693622" width="550" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>	
	</div>	
	
		
				<div class="reservation">
					<div class="container_contact">
							<h1 class="text-center">Reservation</h1>
							<form method="post" action="email.php">
								<div class="form-group">
								  <label for="nom">Name</label>
								  <input type="text" class="form-control" id="nom" aria-describedby="emailHelp" placeholder="Entrer your name">
								</div>
								<div class="form-group">
									<label for="tel">Phone</label>
									<input type="text" class="form-control" id="tel" aria-describedby="emailHelp" placeholder="Entrer your number phone" required>
								  </div>
								<div class="form-group">
									<label for="mail">Mail adress</label>
									<input type="text" class="form-control" id="mail" aria-describedby="emailHelp" placeholder="Entrer your email adress">
									<small id="emailHelp" class="form-text text-muted">We will never communicate your e-mail address</small>
								</div>
			
								<div class="form-group">
									<label for="exampleFormControlTextarea1">Message</label>
									<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Say something.."></textarea>
								</div>
								<button type="submit" class="btn btn-light">Send</button>
							  </form>
					</div>
			
					<footer class="footer">
						<a href="mentionslegales">Legal Notice</a>
						&nbsp;&nbsp;&nbsp;
						<p>© envies de terroir 2019</p>
					</footer>
		</div>
	
		
	</div>	

</div>



            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="https://unpkg.com/scrollreveal"></script>
            <script src="{{ asset('js/main.js') }}" defer></script>
            

    </body>

</html>