@extends('layouts.app')

@section('content')


  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
<div class="card-columns">
        @foreach(App\Formule_Express::all() as $Formule_Express)
        <div class="card">
            <div class="card-header">
                <div class="card-title">Élément de Formule Express</div>
            </div>
            <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><p class="text-center">{{$Formule_Express->nom}}<br>- <br>{{$Formule_Express->nom_anglais}}</p></li>
                    </ul>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><i><p class="text-center">{{$Formule_Express->description}} <br> - <br> {{$Formule_Express->description_anglais}} </p></i></li>
                        @if($Formule_Express->fait_maison == 1)
                        <li class="list-group-item crud"><img src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" width="50" height="50" alt=""></li>
                        @endif                      </ul>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><p class="text-center">{{$Formule_Express->prix}}</li>
                    </ul>

                        <ul class="list-group list-group-flush">
                            <li class="list-group-item crud"><a href="{{ route('Formule_Express.edit',$Formule_Express->id)}}" class="btn btn-primary">Modifier</a>
                            
                                    <form action="{{ route('Formule_Express.destroy', $Formule_Express->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                                              Supprimer
                                            </button>
                                          </form>

                                          

                                        </li>
                            </ul>
            </div>
        </div>
        
        @endforeach
        <a class="nav-link" href="{{ route('Formule_Express.create') }}"><button class="btn btn-success">{{ __("Ajouter un élément") }}</button></a>
</div>



@endsection