@extends('layouts.app')
@section('content')
<div class="container create-menu">
    <h1 class="text-center">CRÉER UN ÉLÉMENT DU MENU</h1>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
        <form action="{{ route('menus.store') }}" method="post">
           
                    @csrf
                
                    <div class="form-group">
                        <label for="nom">Titre</label>
                    <input type="text" id="titre" name="titre" class="form-control" placeholder="Entrez un titre de menu">
                    </div>
        
                    <div class="form-group">
                        <label for="nom_anglais">Prix</label>
                        <input type="text" id="prix" name="prix" class="form-control" placeholder="Entrez un prix de menu" >
                    </div>
            
            <button type="submit" class="btn btn-success">Valider</button>
        </form>
</div>
@endsection
