@extends('layouts.app')

@section('content')


  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
<div class="card-columns">


<div class="card">
  <div class="card-header">
    <div class="card-title">
      <p>Infos du menu</p>
    </div>
  </div>
    @foreach(App\MenuInfo::All() as $menu_info)
    <li class="list-group-item crud"><p>{{$menu_info->titre}}</p></li>
    <li class="list-group-item crud"><p>{{$menu_info->prix}}</p></li>
    <li class="list-group-item crud"><a href="{{ route('menus.edit',$menu_info->id)}}" class="btn btn-primary">Modifier</a>
      <form action="{{ route('menus.destroy', $menu_info->id)}}" method="post">
        @csrf
        @method('DELETE')
         <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#platModal">
          Supprimer
        </button>
      </form>
  @endforeach
  <a class="nav-link" href="{{ route('menus.create') }}"><button class="btn btn-success">{{ __("Ajouter un élément") }}</button></a>

</div>

<div class="card">
  <div class="card-header">
    <div class="card-title"><p>Entrées du menu</p></div>
  </div>
    <div class="card-body">
        @foreach(App\MenuEntree::All() as $menu_entree)
        <li class="list-group-item crud"><p>{{$menu_entree->nom}}</p></li>
        <li class="list-group-item crud"><p>{{$menu_entree->description}}</p></li>
        @if($menu_entree->fait_maison == 1)
        <li class="list-group-item crud"><img src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" width="50" height="50" alt=""></li>
        @endif
        <li class="list-group-item crud"><a href="{{ route('menu_entree.edit',$menu_entree->id)}}" class="btn btn-primary">Modifier</a>
          <form action="{{ route('menu_entree.destroy', $menu_entree->id)}}" method="post">
              @csrf
              @method('DELETE')
               <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#entreeModal">
                Supprimer
              </button>
            </form>
        @endforeach
    </div>
    <a class="nav-link" href="{{ route('menu_entree.create') }}"><button class="btn btn-success">{{ __("Ajouter un élément") }}</button></a>

  
</div>



<div class="card">
    <div class="card-header">
      <div class="card-title"><p>Plats du menu</p></div>
    </div>
      <div class="card-body">
          @foreach(App\MenuPlat::All() as $menu_plat)
          <li class="list-group-item crud"><p>{{$menu_plat->nom}}</p></li>
          <li class="list-group-item crud"><p>{{$menu_plat->description}}</p></li>
          @if($menu_plat->fait_maison == 1)
          <li class="list-group-item crud"><img src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" width="50" height="50" alt=""></li>
          @endif
          <li class="list-group-item crud"><a href="{{ route('menu_plat.edit',$menu_plat->id)}}" class="btn btn-primary">Modifier</a>
            <form action="{{ route('menu_plat.destroy', $menu_plat->id)}}" method="post">
                @csrf
                @method('DELETE')
                 <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#platModal">
                  Supprimer
                </button>
              </form>
          @endforeach
          
      </div>
      <a class="nav-link" href="{{ route('menu_plat.create') }}"><button class="btn btn-success">{{ __("Ajouter un élément") }}</button></a>
  
  </div>


  <div class="card">
      <div class="card-header">
        <div class="card-title"><p>Fromages/Desserts du menu</p></div>
      </div>
        <div class="card-body">
            @foreach(App\menuDessert::All() as $menu_dessert)
            <li class="list-group-item crud"><p>{{$menu_dessert->nom}}</p></li>
            <li class="list-group-item crud"><p>{{$menu_dessert->description}}</p></li>
            @if($menu_dessert->fait_maison == 1)
            <li class="list-group-item crud"><img src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" width="50" height="50" alt=""></li>
            @endif
            <li class="list-group-item crud"><a href="{{ route('menu_dessert.edit',$menu_dessert->id)}}" class="btn btn-primary">Modifier</a>
              <form action="{{ route('menu_dessert.destroy', $menu_dessert->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                   <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#dessertModal">
                    Supprimer
                  </button>
                </form>
              
              
            @endforeach
          </div>
            <a class="nav-link" href="{{ route('menu_dessert.create') }}"><button class="btn btn-success">{{ __("Ajouter un élément") }}</button></a>
        
    
      </div>
    </div>
    
</div>
@endsection