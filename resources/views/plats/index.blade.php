@extends('layouts.app')

@section('content')

@if(session()->get('success'))
<div class="alert alert-success">
  {{ session()->get('success') }}  
</div><br />
@endif
<div class="card-columns">
    @foreach($plats as $plat)
    <div class="card">
        <div class="card-header">
            <div class="card-title">Élément de menu</div>
        </div>
        <div class="card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><p class="text-center">{{$plat->nom}}<br>- <br>{{$plat->nom_anglais}}</p></li>
            </ul>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><i><p class="text-center">{{$plat->description}} <br> - <br> {{$plat->description_anglais}} </p></i></li>
                @if($plat->fait_maison == 1)
                <li class="list-group-item crud"><img src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" width="50" height="50" alt=""></li>
                @endif            
            </ul>
             <ul class="list-group list-group-flush">
                <li class="list-group-item"><i><p class="text-center">{{$plat->prix}}</p></li>
            </ul>
            <ul class="list-group list-group-flush">
                <li class="list-group-item crud"><a href="{{ route('plats.edit',$plat->id)}}" class="btn btn-primary">Modifier</a>
                    
                    <form action="{{ route('plats.destroy', $plat->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                         <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                                              Supprimer
                                            </button>
                                          </form>    

                </li>
            </ul>
        </div>
    </div>
    @endforeach
    <a class="nav-link" href="{{ route('plats.create') }}"><button class="btn btn-success">{{ __("Ajouter un élément") }}</button></a>
</div>


@endsection