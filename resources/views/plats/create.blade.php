@extends('layouts.app')
@section('content')

<div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('plats.store') }}">
          <div class="form-group">
              @csrf
              <label for="nom">Nom:</label>
              <input type="text" class="form-control" name="nom"/>
          </div>
          <div class="form-group">
              <label for="nom_anglais">Nom Anglais :</label>
              <input type="text" class="form-control" name="nom_anglais"/>
          </div>
          <div class="form-group">
              <label for="description">Description:</label>
              <input type="text" class="form-control" name="description"/>
          </div>
          <div class="form-group">
              <label for="description_anglais">Description Anglais :</label>
              <input type="text" class="form-control" name="description_anglais"/>
          </div>
          
          <div class="form-group">
            <label for="select">Fait maison</label>
            <input type="checkbox" value="1" checked id="fait_maison" name="fait_maison">
        </div>

          <div class="form-group">
              <label for="prix">Prix :</label>
              <input type="text" class="form-control" name="prix"/>
          </div>
          <button type="submit" class="btn btn-success">Valider</button>
      </form>
  </div>
</div>

@endsection