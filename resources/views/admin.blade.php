@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tableau de bord</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Bienvenue {{ Auth::user()->name }} !
                </div>
            </div>
        </div>
    </div>
    

    <div class="card" style="width:20rem;">
        <div class="card-header">
            <div class="card-title">
                Mon profil
            </div>
        </div>
        <div class="card-body text-center user">
            <i class="far fa-user-circle fa-6x"></i>
            {{ Auth::user()->name }}
            {{ Auth::user()->email }}

            <a href="reset">Changer mon mot de passe</a>
        </div>
    </div>

   
</div>


@endsection
