
	
	<hr id="about" class="trait">
	<div class="imgroca">

		<div class="titre "> 
			Rocamadour
		</div>
	
				<i class="fas fa-hand-point-up"></i>
			<div class="textroca" id="textroca">
				<p>Un site vertigineux !<br>
					Rocamadour est situé en Vallée de la Dordogne : La cité sacrée est agrippée à la falaise dans une superposition de maisons et de chapelles. Du château qui couronne cette audacieuse construction se dessine un à-pic de quelques 150 mètres au fond duquel serpente le ruisseau de l’Alzou. Sur le chemin de Saint-Jacques-de-Compostelle, la basilique Saint-Sauveur et la crypte Saint-Amadour, classées au Patrimoine Mondial de l’UNESCO, s’offrent aux visiteurs une fois gravies les 216 marches de l’escalier des pèlerins. La chapelle miraculeuse, l’une des 7 autres chapelles bâties au creux du rocher, abrite son joyau, la Vierge Noire vénérée depuis plus d’un millénaire.
				</p>
			</div>	

			<div> </div>
	</div>

