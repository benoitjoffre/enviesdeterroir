<div id="site">
	<header class="header">
		<div class="top">
			<img src="https://image.noelshack.com/fichiers/2019/08/3/1550666908-logoenvt.png">
		</div>
		<hr>
	</header>

	<nav class="navbar navbar-expand-lg" id="navigation">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><i class="fas fa-bars"></i></button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link" href="#" id="accueil">Accueil</a></li>
				<li class="nav-item active"><a class="nav-link" href="#" id="lacarte">La Carte</a></li>
				<li class="nav-item active"><a class="nav-link" href="#" id="rocamadour">Rocamadour</a></li>
				<li class="nav-item active"><a class="nav-link" href="#" id="contatcetreservation">Contact & Réservation</a></li>
				<li class="nav-item active"> <a href="index_anglais"><img src="https://www.stickersmalin.com/images/ajoute/prd/62/62476-image2_448x448.png" alt="" width="20" height="20" style="margin-top:15px;"></a></li>

			</ul>
		</div>
	</nav>
	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		<div class="bgimg">
			<ol class="carousel-indicators">
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img class="d-block w-100" src="https://image.noelshack.com/fichiers/2019/08/4/1550737750-1549985638-envies-de-terroir-038-min.jpg" alt="First slide">
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="https://image.noelshack.com/fichiers/2019/08/4/1550737045-1550063085-envies-de-terroir-060-min.jpg" alt="Second slide">
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="https://image.noelshack.com/fichiers/2019/08/4/1550737046-1549985655-envies-de-terroir-002-min.jpg" alt="Third slide">
				</div>
			</div>
		</div>
		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
		<!-- 
	</div>
	 -->
		<hr>
		<div align="center" class="texteDescriptionRestaurant" media="">
			La brasserie Envies de Terroir vous propose une halte gourmande au coeur du village de Rocamadour. Nous vous acceuillons avec le sourire dans un cadre moderne et avec une cuisine du terroir convaincante. Pour vos plats, nous misons sur la qualité  et la collaboration avec les producteurs locaux.
		</div>
		<hr id="menu">
	</div>
</div>
