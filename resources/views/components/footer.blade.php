<div class="allcontain vertical">
	
	<hr class="trait">
	<div class="logoref">


		<div>
			<a href="https://www.geo.fr/" target="_blank"><img class="geoguide" src="https://image.noelshack.com/fichiers/2019/07/3/1550053417-geo-guide-201512.png" alt="logo geoguide"></a>
		</div>
		<div>
			<a href="https://www.routard.com/" target="_blank"><img class="routard" src="https://image.noelshack.com/fichiers/2019/07/3/1550053468-location-recommande-guide-routard-guadeloupe2.png" alt="logo du routard"></a>
		</div>
		<div>
			<a href="https://www.lonelyplanet.fr/" target="_blank"><img class="lonelyplanet" src="https://image.noelshack.com/fichiers/2019/07/3/1550053530-lonely-planet-svg2.png" alt="logo lonely planet"></a>
		</div>	
		<div>
			<a href="https://www.petitfute.com/" target="_blank"><img class="petitfute" src="https://itctropicar.fr/arn/uploads/2019/01/logo-label-petit-fute.png" alt="logopetit fute"></a>
		</div>
	</div>

	<hr class="trait" id="footer">
	<h1 class="textContact" >Nous contacter & Reserver </h1>

	<div class="contactetmaps">

			<div class="contact">
					<h3 class="text-center"><strong>Horaires</strong></h3>
				<div class="horaires">
					
						<div class="horaireete">
								<h4>Saison</h4>
								<i class="dates">(1er juillet - 31 aout)</i>
								<p>Du lundi au Dimanche</p>
								<p>10h - 22h</p>
								
							</div>
			
							<div class="horairehiver">
								<h4>Hors saison</h4>
								<i class="dates">(1er septembre - 30 juin)</i>
								<p>Tous les jours sauf le vendredi</p>
								<p>10h - 19h</p>
							</div>			
				</div>
				<hr class="traitseparation">
				<h3 class="text-center"><strong>Informations</strong></h3>
				<div class="infos">
						
						<div class="adresse">
								<h4>Adresse</h4>
								<p > Rue de La Couronnerie <br> 46500 Rocamadour </p>
								</div>
				
								<div class="telephone">
								<h4>Telephone</h4>
								<p class="margetel">05 65 33 67 38 </p>
								</div>
				
								
				</div>
				
			
			</div>
	
			<div class="maps">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d20765.69730151013!2d1.6137172714180856!3d44.80171739197678!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd7ff0106bc57a3a!2sEnvies+de+Terroir!5e0!3m2!1sfr!2sfr!4v1549981693622" width="550" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>	
	</div>	
	
		
				<div class="reservation">
					<div class="container_contact">
							<h1 class="text-center">Réservation</h1>
							<form method="post" action="email.php">
								<div class="form-group">
								  <label for="nom">Nom</label>
								  <input type="text" class="form-control" id="nom" aria-describedby="emailHelp" placeholder="Entrez votre nom">
								</div>
								<div class="form-group">
									<label for="tel">Téléphone</label>
									<input type="text" class="form-control" id="tel" aria-describedby="emailHelp" placeholder="Entrez votre numero de telephone" required>
								  </div>
								<div class="form-group">
									<label for="mail">Adresse Mail</label>
									<input type="text" class="form-control" id="mail" aria-describedby="emailHelp" placeholder="Entrez votre email">
									<small id="emailHelp" class="form-text text-muted">Nous ne comuniquerons jamais votre adresse e-mail</small>
								</div>
			
								<div class="form-group">
									<label for="exampleFormControlTextarea1">Message</label>
									<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Écrivez quelque chose..."></textarea>
								</div>
								<button type="submit" class="btn btn-light">Envoyer</button>
							  </form>
					</div>
			
					<footer class="footer">
						<a href="mentionslegales">Mentions légales</a>
						&nbsp;&nbsp;&nbsp;
						<p>© envies de terroir 2019</p>
					</footer>
		</div>
	
		
	</div>	

</div>
