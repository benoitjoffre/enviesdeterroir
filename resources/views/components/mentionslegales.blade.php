<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <script src="{{ asset('js/main.js') }}" defer></script>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Crimson+Text" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <title>Mentions Légales</title>
</head>
<body>
    <div class="containers">
        <h1 class="text-center">MENTIONS LÉGALES</h1>
<div class="mentions_legales">
    <div class="infos_societe">
        <h4>Editeur :</h4>
        <p>ENVIES DE TERROIR</p>
        <p>Rue de la couronnerie 46500 ROCAMADOUR</p>
        <p>Société à responsabilité limitée unipersonnelle <br>au capital social de 250 000,00 €</p>
        <p>enviesdeterroir46@orange.fr</p>
        <p>05 65 33 67 38</p>
        <p>SIREN : 529 066 391 RCS - ROCAMADOUR</p>
    </div>

    <div class="hebergeur">
        <h4>Hébergeur :</h4>
        <p>OVH</p>
        <p>2 rue Kellermann 59100 Roubaix France</p>
    </div>
</div>
    <footer class="footer">
        <a href="/"><p>© envies de terroir 2019</p></a>
    </footer>
    </div>
</body>
</html>