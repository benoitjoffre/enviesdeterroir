<div class="containers" id="menu">
    <h1>La Carte</h1>
    <div class="carteaffichee">
        <div class="formule_express" >
            <div class="titremenu text-center">
                <h2 class="titre">Formule <span>Express</span></h2>
            </div>
            @foreach(App\Formule_Express::All() as $Formule_Express)
            <div class="block">
                <div class="centrercarre">
                    <div class="carrerouge"></div>
                </div>
                <div class="elements_blockformule">
                    <div>
                        <strong>
                            {{$Formule_Express->nom}}, 
                        </strong>
                        {{$Formule_Express->description}}
                    </div>
                    <div class="fait_maison">
                        @if($Formule_Express->fait_maison == 1)
                            <img src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" width="20" height="20" alt="">
                        @endif
                    </div>
                </div>
                <div class="prixformuleexpress text-center">
                    {{$Formule_Express->prix}} €
                </div>
            </div>
            @endforeach
            
            <div class="menu_enfant">
                <div class="titremenuenfant text-center">
                    <h2 class="titre">Menu <span>Enfant</span><span>11€</span></h2>
                </div>
                <div class="elementsmenuenfant">
                    <div class="carrerouge"></div>
                     Steak haché Le Gaillard
                </div>
                <div class="ou">
                    <i><strong>OU</strong></i>
                </div>
                <div class="elementsmenuenfant">
                    <div class="carrerouge"></div>
                     Aiguillettes de canard grillées
                </div>
                <div class="elementsmenuenfant">
                    Pomme frites
                </div>
                <div class="elementsmenuenfant">
                    <div class="carrerouge"></div>
                     Glace (1boule)
                </div>

            </div>

        </div>
        <div class="menu text-center">
            <div class="titremenu">
                @foreach ( App\MenuInfo::All() as $menu_infos)
                <h2 class="titre">{{$menu_infos->titre}} <span>{{$menu_infos->prix}} €</span></h2>    
                @endforeach
            </div>
            @foreach(App\MenuEntree::All() as $menu_entree)
            <div class="block">
                <div class="elements_block">
                    <div>
                        <strong>
                            {{$menu_entree->nom}}
                        </strong>
                    </div>
                    <div class="fait_maison">
                        
                        {{$menu_entree->description}}
                        <br>
                        
                        @if($menu_entree->fait_maison == 1)
                            <img src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" width="20" height="20" alt="">
                        @endif

                    </div>
                </div>
            </div>
            <div>
                
            </div>
        
            @if(!$loop->last)
            Ou
        @endif
            @endforeach
            <div class="aroundcarre">
                <div class="carrerouge"></div>
            </div>


            @foreach(App\MenuPlat::All() as $menu_plat)
            <div class="block">
                <div class="elements_block">
                    <div>
                        <strong>
                            {{$menu_plat->nom}}
                        </strong>
                    </div>
                    <div class="fait_maison">
                        
                        {{$menu_plat->description}}
                        <br>
                        
                        @if($menu_plat->fait_maison == 1)
                            <img src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" width="20" height="20" alt="">
                        @endif
                    </div>
                </div>
            </div>
            <div>
                
            </div>
            @if(!$loop->last)
            Ou
        @endif
            @endforeach

            <div class="aroundcarre">
                <div class="carrerouge"></div>
            </div>
            
            @foreach(App\menuDessert::All() as $menu_dessert)
            <div class="block">
                <div class="elements_block">
                    <div>
                        <strong>
                            {{$menu_dessert->nom}}
                        </strong>
                    </div>
                    <div class="fait_maison">
                        
                        {{$menu_dessert->description}}
                        <br>
                        
                        @if($menu_dessert->fait_maison == 1)
                            <img src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" width="20" height="20" alt="">
                        @endif
                    </div>
                </div>
            </div>
            <div>
                
            </div>
            @if(!$loop->last)
            Ou
        @endif
            @endforeach
            
        </div>
    </div>

<div class="entreesPlats">

    <div class="collapse multi-collapse" id="collapse">
        <div class="entrees">
            <div class="titremenu text-center">
                         <h2 class="titre"> <span> Nos&nbsp; </span> Entrées</h2>
                    </div>

                    @foreach(App\Entrees::All() as $Entrees)
                        <div class="block">
                            <div class="elements_Entrees">

                                <div class="text-centre">
                                    <div class="carrerouge1"></div>
                                    <div><strong> {{$Entrees->nom}} </strong> ..... {{$Entrees->prix}} € </div>
                                        <div class="fait_maison">
                                            <?php
                                                if(isset($Entrees->fait_maison)){
                                                echo'<img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">';
                                                }
                                            ?>
                                        </div>

                                </div>
                                <div>
                                    {{$Entrees->description}}
                                </div>
                            </div>
                        </div>
                    @endforeach
        </div>
        
    </div>


    <div class="collapse multi-collapse" id="collapse">
        <div class="entrees">
            <div class="titremenu text-center">
                         <h2 class="titre text-center"> <span> Nos&nbsp;</span> Plats</h2>
                    </div>

                    @foreach(App\Plat::All() as $Plat)
                        <div class="block">
                            <div class="elements_Entrees">

                                <div class=" text-centre">
                                    <div class="carrerouge1"></div>
                                    <div><strong> {{$Plat->nom}} </strong> ..... {{$Plat->prix}} € </div>
                                        <div class="fait_maison">
                                            <?php
                                                if(isset($Plat->fait_maison)){
                                                echo'<img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">';
                                                }
                                            ?>
                                        </div>

                                </div>
                                <div>
                                    
                                {{$Plat->description}}
                                </div>
                            </div>
                        </div>
                    @endforeach
        </div>
        
    </div>

</div>

    <div class="collapse multi-collapse" id="fromage">
        <div class="fromage">
            <div class="titreFromage">
                <h2 class="titre"><span>Notre&nbsp;</span> Fromage</h2>
            </div>
            <div class="elementsFromage">
                <div class="carrerouge"></div>
                <div>
                     Rocamadour fermier "Lacoste" et salade aux noix ......... 6,00 €
                </div>
            </div>
        </div>
    </div>
    <div class="elementsCollapse">
    <div class="collapse multi-collapse" id="collapse">
        <div class="desserts">
            <div class="titreFromage">
                <h2 class="titre"><span>Nos&nbsp;</span> Desserts</h2>
            </div>
            <div class="carteDesserts">
                <div class="elementsDesserts">
                    <div class="carrerouge"></div>
                    <div>
                        <strong>Pruneaux au vin rouge</strong>........5,00 €
                    </div>
                    <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                    
                </div>
                    <div>
                        et sa boule de glace
                    </div>
                <div class="elementsDesserts">
                    <div class="carrerouge"></div>
                    <div>
                        <strong>Succes aux noix</strong>........5,50 €
                    </div>
                    <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                    
                </div>
                <div class="elementsDesserts">
                    <div class="carrerouge"></div>
                    <div>
                        <strong>Tarte aux mirtilles</strong>........5,90 €
                    </div>
                </div>
                <div class="elementsDesserts">
                    <div class="carrerouge"></div>
                    <div>
                        <strong>Tarte aux pommes tièdes</strong>........6,50 €
                    </div>
                    <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                    
                </div>
                    <div>
                        et sa boule de glace vanille
                    </div>
                <div class="elementsDesserts">
                    <div class="carrerouge"></div>
                    <div>
                        <strong>Riz au lait</strong>........5,60 €
                    </div>
                    <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                    
                </div>
                    <div>
                        caramel beurre salé ou fruits rouges
                    </div>
                <div class="elementsDesserts">
                    <div class="carrerouge"></div>
                    <div>
                        <strong>Pannacotta façon</strong>........5,90 €
                    </div>
                    <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                    
                </div>
                    <div>
                        chocolat liègois
                    </div>
            </div>
        </div>
    </div>
    <div class="collapse multi-collapse" id="collapse">
        <div class="crepes">
            <div class="titreFromage">
                <h2 class="titre"><span>Nos&nbsp;</span>Crèpes </h2>
            </div>
            <div class="carteCrepe">
                <div class="elementsDesserts">
                    <div class="carrerouge"></div>
                    <div>
                        <strong>Sucre</strong>........2,90 €
                    </div>
                    <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                </div>
                <div class="elementsDesserts">
                    <div class="carrerouge"></div>
                    <div>
                        <strong>Sucre, citron</strong>........4,20 €
                    </div>
                    <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                </div>
                <div class="elementsDesserts">
                    <div class="carrerouge"></div>
                    <div>
                        <strong>Confiture</strong>........3,90 €
                    </div>
                    <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                </div>
                <div class="elementsDesserts">
                    <div class="carrerouge"></div>
                    <div>
                        <strong>Nutella</strong>........4,70 €
                    </div>
                    <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                </div>
                <div class="elementsDesserts">
                    <div class="carrerouge"></div>
                    <div>
                        <strong>Caramel beurre salé</strong>........6,00 €
                    </div>
                    <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                    
                </div>
                    <div>
                        et sa boule de glace vanille
                    </div>
                <div class="elementsDesserts">
                    <div class="carrerouge"></div>
                    <div>
                        <strong>Grand Marnier</strong>........6,00 €
                    </div>
                    <img class="faitMaison" src="https://monkeychamonix.com/site/wp-content/uploads/2014/08/icon_60175.png" alt="Fais maison">
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="row justify-content-center">
        <button class="btn btn-secondary plus" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="collapse fromage">Voir l'intégralité de La Carte</button>
    </div>
</div>