@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Vérifiez votre adresse e-mail') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Un lien de vérification vous a été envoyé par e-mail.') }}
                        </div>
                    @endif

                    {{ __('Avant de vous connecter, veuillez vérifier vos mails pour le lien de vérification.') }}
                    {{ __("Si vous n'avez pas reçu de mail") }}, <a href="{{ route('verification.resend') }}">{{ __('Cliquez ici pour en recevoir un nouveau') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
