@extends('layouts.app')
@section('content')
<div class="container create-dessert">
    <h1 class="text-center">MODIFIER UN DESSERT DU MENU</h1>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
        <form action="{{ route('menu_dessert.update', $menu_dessert->id) }}" method="post">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="nom">Nom</label>
            <input type="text" id="nom" name="nom" class="form-control" placeholder="Entrez un élément de dessert" value="{{$menu_dessert->nom}}">
            </div>

            <div class="form-group">
                <label for="nom_anglais">Nom (traduction anglaise)</label>
                <input type="text" id="nom_anglais" name="nom_anglais" class="form-control" placeholder="Entrez un élément de dessert en anglais" value="{{$menu_dessert->nom_anglais}}">
            </div>
            
            <div class="form-group">
                <label for="description">Description</label>
                <input type="text" id="description" name="description" class="form-control" placeholder="Entrez une description" value="{{$menu_dessert->description}}">
            </div>


            <div class="form-group">
                <label for="description_anglais">Description (traduction anglaise)</label>
                <input type="text" id="description_anglais" name="description_anglais" class="form-control" placeholder="Entrez une description en anglais" value="{{$menu_dessert->description_anglais}}">
            </div>
           
            <div class="form-group">
                <label for="select">Fait maison</label>
                <input type="checkbox" value="1" checked id="fait_maison" name="fait_maison">
            </div>

            <button type="submit" class="btn btn-success">Valider</button>
        </form>
</div>
@endsection