<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuEntreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_entrees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->string('nom_anglais')->nullable();
            $table->string('description')->nullable();
            $table->string('description_anglais')->nullable();
            $table->boolean('fait_maison')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_entrees');
    }
}
