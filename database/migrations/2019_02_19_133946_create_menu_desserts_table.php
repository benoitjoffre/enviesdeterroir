<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuDessertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_desserts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->string('nom_anglais');
            $table->string('description')->nullable();
            $table->string('description_anglais')->nullable();
            $table->boolean('fait_maison')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_desserts');
    }
}
